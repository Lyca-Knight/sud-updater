const classname = document.getElementsByClassName("updater");
const BrowserWindow = require('electron').remote.BrowserWindow
const path = require('path');
const spawn = require('child_process').spawn;

var windowOptions = {
  width: 800,
  height: 600,
  frame: false,
  minimizable: false,
  maximizable: false,
  resizable: false,
  modal: true,
  parent: BrowserWindow.MainWindow,
  title: "SuD Updater"
}

var updater = function () {
  winupdate = new BrowserWindow(windowOptions)
  winupdate.on('close', function () { win = null })
  winupdate.loadURL(path.join('file://', __dirname, '../../index_updater.html'))
  winupdate.show()
};

for (var i = 0; i < classname.length; i++) {
  classname[i].addEventListener('click', updater, false);
}
